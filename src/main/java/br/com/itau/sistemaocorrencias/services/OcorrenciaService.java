package br.com.itau.sistemaocorrencias.services;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;

import br.com.itau.sistemaocorrencias.enums.Categoria;
import br.com.itau.sistemaocorrencias.enums.Nivel;
import br.com.itau.sistemaocorrencias.models.Ocorrencia;
import br.com.itau.sistemaocorrencias.repositories.OcorrenciaRepository;

@Service
public class OcorrenciaService {

	@Autowired
	private OcorrenciaRepository ocorrenciaRepository;

	public Ocorrencia setOcorrencia(Ocorrencia ocorrencia) {
		return ocorrenciaRepository.save(ocorrencia);
	}

	public Ocorrencia getOcorrencia(int id) {
		Optional<Ocorrencia> optionalOcorrencia = ocorrenciaRepository.findById(id);

		if (!optionalOcorrencia.isPresent()) {
			throw new ResponseStatusException(HttpStatus.NOT_FOUND, "Não existe ocorrência para o ID fornecido!");
		}

		return optionalOcorrencia.get();
	}

	public Iterable<Ocorrencia> getOcorrencias() {
		return ocorrenciaRepository.findAll();
	}


	public void resolveOcorrencia(int id) {
		Optional<Ocorrencia> optionalOcorrencia = ocorrenciaRepository.findById(id);
		
	    // Checa se o ID corresponde a uma entrada no BD
		if (!optionalOcorrencia.isPresent()) {
			throw new ResponseStatusException(HttpStatus.NOT_FOUND, "Não existe ocorrência para o ID fornecido!");
		}

		Ocorrencia ocorrencia = optionalOcorrencia.get();

		// Checa se a ocorrencia já foi marcada como resolvida
		if (ocorrencia.isResolvido()) {
			throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "A ocorrência já foi resolvida!");
		}
		
		ocorrencia.setResolvido(true);
		ocorrenciaRepository.save(ocorrencia);

	}	
	public Iterable<Ocorrencia> getOcorrenciasPorCategoria(String nomeCategoria) {

		Categoria categoriaLida = null;

		for(Categoria categoria: Categoria.values()) {
			if(categoria.name().equals(nomeCategoria.toUpperCase())){
				categoriaLida = categoria;
			}
		}

		if (categoriaLida == null) {
			throw new ResponseStatusException(HttpStatus.NOT_ACCEPTABLE, "Categoria inválida!");
		}

		return ocorrenciaRepository.findByCategoria(categoriaLida);
	}
	
	public Iterable<Ocorrencia> getOcorrenciasPorNivel(String nomeNivel) {
	
		Nivel nivelLido = null;

		for(Nivel nivel: Nivel.values()) {
			if(nivel.name().equals(nomeNivel.toUpperCase())){
				nivelLido = nivel;
			}
		}

		if (nivelLido == null) {
			throw new ResponseStatusException(HttpStatus.NOT_ACCEPTABLE, "Nivel inválido!");
		}

		return ocorrenciaRepository.findByNivel(nivelLido);
	}
	
	public Iterable<Ocorrencia> getOcorrenciasPorCategoriaENivel(String nomeCategoria, String nomeNivel) {
		Categoria categoriaLida = null;
		Nivel nivelLido = null;

		for(Categoria categoria: Categoria.values()) {
			if(categoria.name().equals(nomeCategoria.toUpperCase())){
				categoriaLida = categoria;
			}
		}

		for(Nivel nivel: Nivel.values()) {
			if(nivel.name().equals(nomeNivel.toUpperCase())){
				nivelLido = nivel;
			}
		}

		if (categoriaLida == null) {
			throw new ResponseStatusException(HttpStatus.NOT_ACCEPTABLE, "Categoria inválida!");
		}
		if (nivelLido == null) {
			throw new ResponseStatusException(HttpStatus.NOT_ACCEPTABLE, "Nivel inválido!");
		}

		
		return ocorrenciaRepository.findByCategoriaAndNivel(categoriaLida, nivelLido);
	}
}
