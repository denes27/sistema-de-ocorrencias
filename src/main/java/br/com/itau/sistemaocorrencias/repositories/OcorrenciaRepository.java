package br.com.itau.sistemaocorrencias.repositories;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import br.com.itau.sistemaocorrencias.enums.Categoria;
import br.com.itau.sistemaocorrencias.enums.Nivel;
import br.com.itau.sistemaocorrencias.models.Ocorrencia;

@Repository
public interface OcorrenciaRepository extends CrudRepository<Ocorrencia, Integer>{
	  Iterable<Ocorrencia> findByCategoria(Categoria categoria);
	  
	  Iterable<Ocorrencia> findByNivel(Nivel nivel);
	  
	  Iterable<Ocorrencia> findByCategoriaAndNivel(Categoria categoria, Nivel nivel);
}

