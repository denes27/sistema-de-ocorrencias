package br.com.itau.sistemaocorrencias.controllers;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import com.fasterxml.jackson.databind.ObjectMapper;

import br.com.itau.sistemaocorrencias.dtos.LoginDto;
import br.com.itau.sistemaocorrencias.dtos.TokenDto;
import br.com.itau.sistemaocorrencias.enums.Autoridade;
import br.com.itau.sistemaocorrencias.models.Usuario;
import br.com.itau.sistemaocorrencias.services.UsuarioService;


@RunWith(SpringRunner.class)
@WebMvcTest(controllers = UsuarioController.class)
public class UsuarioControllerTest {

	@Autowired
	private MockMvc mockMvc;
	

	@MockBean
	private UsuarioService usuarioService;
	
	private ObjectMapper mapper = new ObjectMapper();
	private Usuario usuario;
	
	@Before
	public void preparar() {
		usuario = new Usuario();
		usuario.setAutoridade(Autoridade.USUARIO);
		usuario.setContato("teste@teste.com");
		usuario.setCpf("123.456.789-10");
		usuario.setId(1);
		usuario.setNome("testevaldo dos testes");
		usuario.setSenha("teste123");
	}
	
	
	
	@Test
	public void deveChamarEndpointdeLoginEAutenticarSeSenhaForValida() throws Exception {
		LoginDto login = new LoginDto();
		login.setCpf(usuario.getCpf());
		login.setSenha(usuario.getSenha());
		
		TokenDto tokenDto = new TokenDto();
		tokenDto.setUsuario(usuario.getCpf());
		tokenDto.setToken("token só que é um hash");
		
		when(usuarioService.login(any(LoginDto.class))).thenReturn(tokenDto);
		String content = mapper.writeValueAsString(login);
		mockMvc.perform(post("/usuario/login").content(content).contentType(MediaType.APPLICATION_JSON_UTF8))
			.andExpect(status().isOk());
	}
 
	  @Test
	  public void deveCriarUmUsuario() throws Exception {
	    when(usuarioService.setUsuario(any(Usuario.class))).thenReturn(usuario);
	    
	    String usuarioJson = mapper.writeValueAsString(usuario);
	    
	    mockMvc.perform(post("/usuario/cadastrar")
	            .contentType(MediaType.APPLICATION_JSON_UTF8)
	            .content(usuarioJson))
	        .andExpect(status().isCreated())
	        .andExpect(content().string(usuarioJson));
	  }
}
