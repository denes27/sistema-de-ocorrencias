package br.com.itau.sistemaocorrencias.services;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpStatus;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.web.server.ResponseStatusException;

import br.com.itau.sistemaocorrencias.enums.Categoria;
import br.com.itau.sistemaocorrencias.enums.Nivel;
import br.com.itau.sistemaocorrencias.models.Coordenadas;
import br.com.itau.sistemaocorrencias.models.Ocorrencia;
import br.com.itau.sistemaocorrencias.repositories.OcorrenciaRepository;

@RunWith(SpringRunner.class)
@ContextConfiguration(classes = OcorrenciaService.class)
public class OcorrenciaServiceTest {

	@Autowired
	private OcorrenciaService subject;

	@MockBean
	private OcorrenciaRepository ocorrenciaRepository;

	private Coordenadas coordenadas;
	private Ocorrencia ocorrencia;

	@Before
	public void setUp() {
		coordenadas = new Coordenadas();
		coordenadas.setLatitude(-10.0);
		coordenadas.setLongitude(-10.0);

		ocorrencia = new Ocorrencia();
		ocorrencia.setId(123);
		ocorrencia.setTitulo("Alagamento no Bairro das Flores");
		ocorrencia.setDescricao("");
		ocorrencia.setNivel(Nivel.ALTO);
		ocorrencia.setCategoria(Categoria.DESASTRE);
		ocorrencia.setData(null);
		ocorrencia.setResolvido(false);
		ocorrencia.setCoordenadas(coordenadas);
	}

	// Tests: setOcorrencia()
	@Test
	public void deveGravarUmaOcorrenciaNaBase() {
		when(ocorrenciaRepository.save(ocorrencia)).thenReturn(ocorrencia);
		Ocorrencia retorno = subject.setOcorrencia(ocorrencia);
		assertEquals(ocorrencia, retorno);
	}

	// Tests: getOcorrencia()
	@Test
	public void deveRetornarUmaOcorrencia() {
		// When
		Mockito.when(ocorrenciaRepository.findById(ocorrencia.getId())).thenReturn(Optional.of(ocorrencia));
		Ocorrencia ocorrenciaRetorno = subject.getOcorrencia(ocorrencia.getId());

		// Expected
		assertEquals(ocorrencia, ocorrenciaRetorno);
	}

	@Test
	public void deveLancarExcecaoQuandoTentarRetornarOcorrenciaNaoExistente() {
		// When
		when(ocorrenciaRepository.findById(ocorrencia.getId())).thenReturn(Optional.empty());

		try {
			subject.getOcorrencia(ocorrencia.getId());
		} catch (ResponseStatusException e) {
			assertEquals(HttpStatus.NOT_FOUND, e.getStatus()); 
		}
	}

	// Tests: getOcorrencias()
	@Test
	public void deveRetornarTodasAsOcorrencias() {
		// Given
		List<Ocorrencia> ocorrencias = new ArrayList<>();
		ocorrencias.add(ocorrencia);
		ocorrencias.add(ocorrencia);
		ocorrencias.add(ocorrencia);

		// When
		Mockito.when(ocorrenciaRepository.findAll()).thenReturn(ocorrencias);
		Iterable<Ocorrencia> ocorrenciasRetorno = subject.getOcorrencias();

		// Expected
		assertEquals(ocorrencias, ocorrenciasRetorno);

	}

	// Tests: resolveOcorrencia()
	@Test
	public void deveResolverUmaOcorrencia() {
		// When
		Mockito.when(ocorrenciaRepository.findById(ocorrencia.getId())).thenReturn(Optional.of(ocorrencia));
		Mockito.when(ocorrenciaRepository.save(ocorrencia)).thenReturn(ocorrencia);

		subject.resolveOcorrencia(ocorrencia.getId());

		// Expected
		verify(ocorrenciaRepository).save(any(Ocorrencia.class));
		assertTrue(ocorrencia.isResolvido());
	}
	
	@Test
	public void deveLancarExcecaoQuandoTentarResolverOcorrenciaNaoExistente() {
		// When
		when(ocorrenciaRepository.findById(ocorrencia.getId())).thenReturn(Optional.empty());

		try {
			subject.resolveOcorrencia(ocorrencia.getId());
		} catch (ResponseStatusException e) {
			assertEquals(HttpStatus.NOT_FOUND, e.getStatus()); 
		}
	}
	
	@Test
	public void deveLancarExcecaoQuandoTentarResolverOcorrenciaJaResolvida() {
		// Given
		ocorrencia.setResolvido(true);

		// When
		when(ocorrenciaRepository.findById(ocorrencia.getId())).thenReturn(Optional.of(ocorrencia));

		try {
			subject.resolveOcorrencia(ocorrencia.getId());
		} catch (ResponseStatusException e) {
			assertEquals(HttpStatus.BAD_REQUEST, e.getStatus()); 
		}
	}
	
	@Test
	public void deveRetornarTodasAsOcorrenciasPorCategoria(){
		// Given
		List<Ocorrencia> ocorrencias = new ArrayList<>();

		ocorrencias.add(ocorrencia);
		ocorrencias.add(ocorrencia);
		ocorrencias.add(ocorrencia);
		
		// When
		Mockito.when(ocorrenciaRepository.findByCategoria(ocorrencia.getCategoria()))
					.thenReturn(ocorrencias);
		Iterable<Ocorrencia> ocorrenciasRetornoCategoria = 
				subject.getOcorrenciasPorCategoria(ocorrencia.getCategoria().name());

		// Expected
		assertEquals(ocorrencias, ocorrenciasRetornoCategoria);
	}
		
	@Test
	public void deveRetornarTodasAsOcorrenciasPorNivel(){
		// Given
		List<Ocorrencia> ocorrencias = new ArrayList<>();

		ocorrencias.add(ocorrencia);
		ocorrencias.add(ocorrencia);
		ocorrencias.add(ocorrencia);
		
		// When
		Mockito.when(ocorrenciaRepository.findByNivel(ocorrencia.getNivel()))
					.thenReturn(ocorrencias);
		Iterable<Ocorrencia> ocorrenciasRetornoNivel = 
				subject.getOcorrenciasPorNivel(ocorrencia.getNivel().name());

		// Expected
		assertEquals(ocorrencias, ocorrenciasRetornoNivel);
	}	
}
