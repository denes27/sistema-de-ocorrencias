package br.com.itau.sistemaocorrencias.enums;

public enum Nivel {
	BAIXO,
	MEDIO,
	ALTO;
}
