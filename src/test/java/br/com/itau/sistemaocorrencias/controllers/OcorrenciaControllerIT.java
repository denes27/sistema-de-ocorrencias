package br.com.itau.sistemaocorrencias.controllers;

import static org.hamcrest.CoreMatchers.containsString;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import javax.transaction.Transactional;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import com.fasterxml.jackson.databind.ObjectMapper;
import br.com.itau.sistemaocorrencias.enums.Categoria;
import br.com.itau.sistemaocorrencias.enums.Nivel;
import br.com.itau.sistemaocorrencias.models.Coordenadas;
import br.com.itau.sistemaocorrencias.models.Ocorrencia;

@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
@Transactional
@Sql(scripts = "classpath:dados-it-ocorrencia.sql")
public class OcorrenciaControllerIT {

	@Autowired
	private MockMvc mockMvc;
	private Ocorrencia ocorrencia;
	private ObjectMapper mapper = new ObjectMapper();

	@Before
	public void preparar() {
		Coordenadas coordenadas = new Coordenadas();
		coordenadas.setLatitude(-10.0);
		coordenadas.setLongitude(-10.0);
		ocorrencia = new Ocorrencia();
		ocorrencia.setCategoria(Categoria.INFRAESTRUTURA);
		ocorrencia.setCoordenadas(coordenadas);
		ocorrencia.setDescricao("Titulo da ocorrencia");
		ocorrencia.setNivel(Nivel.ALTO);
		ocorrencia.setResolvido(false);
		ocorrencia.setTitulo("Descricao da ocorrencia");
	}

	@WithMockUser
	@Test
	public void deveCriarOcorrencia() throws Exception {
		String content = mapper.writeValueAsString(ocorrencia);
		mockMvc.perform(post("/ocorrencia").content(content).contentType(MediaType.APPLICATION_JSON_UTF8))
				.andExpect(status().isCreated()).andExpect(content().string(containsString(ocorrencia.getTitulo())));
	}

	@Test
	public void deveBuscarTodasAsOcorrencias() throws Exception {
		mockMvc.perform(get("/ocorrencia/listar")).andExpect(status().isAccepted())
				.andExpect(content().string(containsString(ocorrencia.getTitulo())));
	}
}
