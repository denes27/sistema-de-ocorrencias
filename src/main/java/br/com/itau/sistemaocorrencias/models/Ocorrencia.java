package br.com.itau.sistemaocorrencias.models;

import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.validation.constraints.NotNull;

import org.hibernate.annotations.CreationTimestamp;

import br.com.itau.sistemaocorrencias.enums.Categoria;
import br.com.itau.sistemaocorrencias.enums.Nivel;

@Entity
public class Ocorrencia {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private int Id;
	
	@CreationTimestamp
	@Column(name = "Data")
	private LocalDateTime data;
	
	@Column(name = "Categoria", nullable = false)
	@NotNull(message="Categoria da ocorrência não inserida!")
	private Categoria categoria;
	
	@NotNull(message="Coordenadas da ocorrência não inseridas!")
	private Coordenadas coordenadas;
	
	@Column(name = "Nivel", nullable = false)
	@NotNull(message="Nível da ocorrência não inserida!")
	private Nivel nivel;
	
	@Column(name = "Resolvido", nullable = false)
	private boolean resolvido;
	
	@Column(name = "Titulo", nullable = false)
	@NotNull(message="Título da ocorrência não inserida!")
	private String titulo;
	
	@Column(name = "Descricao")
	@NotNull(message="Descrição da ocorrência não inserida!")
	private String descricao;
	
	public int getId() {
		return Id;
	}
	public void setId(int id) {
		Id = id;
	}
	public LocalDateTime getData() {
		return data;
	}
	public void setData(LocalDateTime data) {
		this.data = data;
	}
	public Categoria getCategoria() {
		return categoria;
	}
	public void setCategoria(Categoria categoria) {
		this.categoria = categoria;
	}
	public Coordenadas getCoordenadas() {
		return coordenadas;
	}
	public void setCoordenadas(Coordenadas coordenadas) {
		this.coordenadas = coordenadas;
	}
	public Nivel getNivel() {
		return nivel;
	}
	public void setNivel(Nivel nivel) {
		this.nivel = nivel;
	}
	public boolean isResolvido() {
		return resolvido;
	}
	public void setResolvido(boolean resolvido) {
		this.resolvido = resolvido;
	}
	public String getTitulo() {
		return titulo;
	}
	public void setTitulo(String titulo) {
		this.titulo = titulo;
	}
	public String getDescricao() {
		return descricao;
	}
	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}
}
