package br.com.itau.sistemaocorrencias.services;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;

import br.com.itau.sistemaocorrencias.dtos.LoginDto;
import br.com.itau.sistemaocorrencias.dtos.TokenDto;
import br.com.itau.sistemaocorrencias.models.Usuario;
import br.com.itau.sistemaocorrencias.repositories.UsuarioRepository;
import br.com.itau.sistemaocorrencias.security.JwtTokenProvider;

@Service
public class UsuarioService {

	@Autowired
	private UsuarioRepository usuarioRepository;

	@Autowired
	private BCryptPasswordEncoder bCrypt;
	
	public TokenDto login(LoginDto login) {
		Optional<Usuario> findByCpf = usuarioRepository.findByCpf(login.getCpf());
		if(findByCpf.isPresent()) {
			Usuario usuario = findByCpf.get();
			String senhaArmazenada = usuario.getSenha();
			if(bCrypt.matches(login.getSenha(), senhaArmazenada)) {
				JwtTokenProvider tokenProvider = new JwtTokenProvider();
				TokenDto tokenDto = new TokenDto();
				tokenDto.setToken(tokenProvider.criarToken(usuario.getCpf(), usuario.getAutoridade()));
				tokenDto.setUsuario(login.getCpf());
				return tokenDto;
			}else {
			    throw new ResponseStatusException(HttpStatus.FORBIDDEN, "Falha na tentativa de login - Senha inválida!");
			}
		}else {
			throw new ResponseStatusException(HttpStatus.FORBIDDEN, "Falha na tentativa de login - Usuário não existente!");
		}
	}

	public Usuario setUsuario(Usuario usuario) {
		
		// Checa se o usuário já existe no sistema
		Optional<Usuario> findByCpf = usuarioRepository.findByCpf(usuario.getCpf());
		if(findByCpf.isPresent()) {
			throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Cliente com o CPF informado já existe!");
		}

		usuario.setSenha(bCrypt.encode(usuario.getSenha()));
		return usuarioRepository.save(usuario);

	}
}
