package br.com.itau.sistemaocorrencias.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

import br.com.itau.sistemaocorrencias.enums.Autoridade;
import br.com.itau.sistemaocorrencias.security.JwtFilter;

@Configuration
@EnableWebSecurity
public class WebSecurityConfig extends WebSecurityConfigurerAdapter {
	@Override
	protected void configure(HttpSecurity http) throws Exception {
		http.csrf().disable()
			.sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS)
			.and().authorizeRequests()
				.antMatchers(HttpMethod.POST, "/ocorrencia").authenticated()
				.antMatchers(HttpMethod.DELETE, "/ocorrencia/*").hasAuthority(Autoridade.ADMIN.name())
				.anyRequest().permitAll()
			.and()
			.addFilterBefore(new JwtFilter(), UsernamePasswordAuthenticationFilter.class);
	}

}