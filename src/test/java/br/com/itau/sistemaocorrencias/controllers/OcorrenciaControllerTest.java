package br.com.itau.sistemaocorrencias.controllers;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.List;

import org.assertj.core.util.Lists;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import com.fasterxml.jackson.databind.ObjectMapper;

import br.com.itau.sistemaocorrencias.enums.Categoria;
import br.com.itau.sistemaocorrencias.enums.Nivel;
import br.com.itau.sistemaocorrencias.models.Coordenadas;
import br.com.itau.sistemaocorrencias.models.Ocorrencia;
import br.com.itau.sistemaocorrencias.services.OcorrenciaService;

@RunWith(SpringRunner.class)
@WebMvcTest(controllers = OcorrenciaController.class)
public class OcorrenciaControllerTest {

	private Ocorrencia ocorrencia;
	private Coordenadas coordenadas;
	
	private ObjectMapper mapper = new ObjectMapper();

	@Autowired
	private MockMvc mockMvc;

	@MockBean
	private OcorrenciaService service;

	@Before
	public void preparar() {
		coordenadas = new Coordenadas();
		coordenadas.setLatitude(-10.0);
		coordenadas.setLongitude(-10.0);
		ocorrencia = new Ocorrencia();
		ocorrencia.setCategoria(Categoria.INFRAESTRUTURA);
		ocorrencia.setCoordenadas(coordenadas);
		ocorrencia.setDescricao("Descricao da ocorrencia");
		ocorrencia.setNivel(Nivel.ALTO);
		ocorrencia.setResolvido(false);
		ocorrencia.setTitulo("Ocorrencia de Teste");
	}

	@Test
	@WithMockUser
	public void deveChamarEndpointDeCadastroDeOcorrenciasEGravarUmaOcorrencia() throws Exception {
		String content = mapper.writeValueAsString(ocorrencia);
		Mockito.when(service.setOcorrencia(any(Ocorrencia.class))).thenReturn(ocorrencia);
		mockMvc.perform(MockMvcRequestBuilders.post("/ocorrencia").content(content)
				.contentType(MediaType.APPLICATION_JSON_UTF8)).andExpect(status().isCreated())
				.andExpect(content().string(content));
	}

	@Test
	public void deveChamarEndpointDeCadastroDeOcorrenciasEFalharSeNaoEstiverLogado() throws Exception {
		String content = mapper.writeValueAsString(ocorrencia);
		Mockito.when(service.setOcorrencia(any(Ocorrencia.class))).thenReturn(ocorrencia);
		mockMvc.perform(MockMvcRequestBuilders.post("/ocorrencia").content(content)
				.contentType(MediaType.APPLICATION_JSON_UTF8)).andExpect(status().isForbidden());
	}

	@Test
	public void deveBuscarTodasAsOcorrencias() throws Exception {
		List<Ocorrencia> ocorrencias = Lists.list(ocorrencia);

		when(service.getOcorrencias()).thenReturn(ocorrencias);
 
		mockMvc.perform(get("/ocorrencia/listar"))
				.andExpect(status().isAccepted())
				.andExpect(content().string(mapper.writeValueAsString(ocorrencias)));
	}

	@Test
	public void deveBuscarTodasAsOcorrenciasPorCategoria() throws Exception {
		List<Ocorrencia> ocorrencias = Lists.list(ocorrencia);

		when(service.getOcorrenciasPorCategoria(ocorrencia.getCategoria().name())).thenReturn(ocorrencias);

		mockMvc.perform(get("/ocorrencia/listar?categoria=INFRAESTRUTURA"))
				.andExpect(status().isAccepted())
				.andExpect(content().string(mapper.writeValueAsString(ocorrencias)));
	}
	
	  @Test
	  public void deveBuscarTodasAsOcorrenciasPorNivel() throws Exception { 
	    List<Ocorrencia> ocorrencias = Lists.list(ocorrencia);
	    
	    when(service.getOcorrenciasPorNivel(ocorrencia.getNivel().name())).thenReturn(ocorrencias);
	    
	    mockMvc.perform(get("/ocorrencia/listar?nivel=ALTO"))
	            .andExpect(status().isAccepted())
	            .andExpect(content().string(mapper.writeValueAsString(ocorrencias)));
	  }
	
	  @Test
	  public void deveBuscarTodasAsOcorrenciasPorCategoriaENivel() throws Exception { 
	    List<Ocorrencia> ocorrencias = Lists.list(ocorrencia);
	   
	    when(service.getOcorrenciasPorCategoriaENivel(ocorrencia.getCategoria().name(), ocorrencia.getNivel().name())).thenReturn(ocorrencias);
	    
	    mockMvc.perform(get("/ocorrencia/listar?categoria=INFRAESTRUTURA&nivel=ALTO"))
	            .andExpect(status().isAccepted())
	            .andExpect(content().string(mapper.writeValueAsString(ocorrencias)));
	  }
}
