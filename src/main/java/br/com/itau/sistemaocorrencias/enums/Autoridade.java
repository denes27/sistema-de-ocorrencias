package br.com.itau.sistemaocorrencias.enums;

public enum Autoridade {
	USUARIO,
	ADMIN;
}
