package br.com.itau.sistemaocorrencias.controllers;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.HashMap;

import javax.transaction.Transactional;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import com.fasterxml.jackson.databind.ObjectMapper;

@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
@Transactional
@Sql(scripts = "classpath:dados-it-usuario.sql")
public class UsuarioControllerTestIT {
	  @Autowired
	  private MockMvc mockMvc;
	  
	  private ObjectMapper mapper = new ObjectMapper();
	  
	  @WithMockUser
	  @Test
	  public void deveSalvarUmUsuario() throws Exception {
	    HashMap<String, String> stringonaDaVida = new HashMap<>();
	    stringonaDaVida.put("senha", "123456");
	    
	    String usuarioJson = mapper.writeValueAsString(stringonaDaVida);
	    
	    mockMvc.perform(
	        post("/usuario/cadastrar")
	        .content(usuarioJson)
	        .contentType(MediaType.APPLICATION_JSON_UTF8)
	       )
	      .andExpect(status().isCreated());
	    }

}
