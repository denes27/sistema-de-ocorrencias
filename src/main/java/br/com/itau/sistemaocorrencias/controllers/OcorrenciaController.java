package br.com.itau.sistemaocorrencias.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import br.com.itau.sistemaocorrencias.models.Ocorrencia;
import br.com.itau.sistemaocorrencias.services.OcorrenciaService;

@RestController
@RequestMapping("/ocorrencia")
@CrossOrigin
public class OcorrenciaController {

	@Autowired
	private OcorrenciaService ocorrenciaService;

	@PostMapping
	@ResponseStatus(code = HttpStatus.CREATED)
	public Ocorrencia setOcorrencia(@RequestBody Ocorrencia ocorrencia) {
		return ocorrenciaService.setOcorrencia(ocorrencia);
	}

	@GetMapping("/listar")
	@ResponseStatus(code = HttpStatus.ACCEPTED)
	public Iterable<Ocorrencia> getOcorrencias(@RequestParam(name = "categoria", required = false) String nomeCategoria,
											   @RequestParam(name = "nivel", required = false) String nomeNivel) {

		// Todas as ocorrencias por categoria e nível
		if (nomeCategoria != null && nomeNivel != null) {
			return ocorrenciaService.getOcorrenciasPorCategoriaENivel(nomeCategoria, nomeNivel);
		}
		// Todas as ocorrencias por categoria
		if (nomeCategoria != null && nomeNivel == null) {
			return ocorrenciaService.getOcorrenciasPorCategoria(nomeCategoria);
		}
		// Todas as ocorrencias por nível
		if (nomeCategoria == null && nomeNivel != null) {
			return ocorrenciaService.getOcorrenciasPorNivel(nomeNivel);
		}
		// Todas as ocorrencias
		return ocorrenciaService.getOcorrencias();
	}

	@GetMapping("/{id}")
	@ResponseStatus(code = HttpStatus.ACCEPTED)
	public Ocorrencia getOcorrencia(@PathVariable int id) {
		return ocorrenciaService.getOcorrencia(id);
	}

	@DeleteMapping("/{id}")
	@ResponseStatus(code = HttpStatus.ACCEPTED)
	public void resolveOcorrencia(@PathVariable int id) {
		ocorrenciaService.resolveOcorrencia(id);
	}
}
