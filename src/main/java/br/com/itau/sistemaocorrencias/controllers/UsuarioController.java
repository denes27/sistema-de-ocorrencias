package br.com.itau.sistemaocorrencias.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org. springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import br.com.itau.sistemaocorrencias.dtos.LoginDto;
import br.com.itau.sistemaocorrencias.dtos.TokenDto;
import br.com.itau.sistemaocorrencias.models.Usuario;
import br.com.itau.sistemaocorrencias.services.UsuarioService;

@RestController
@RequestMapping("/usuario")
@CrossOrigin
public class UsuarioController {

	@Autowired
	private UsuarioService usuarioService;
	@ResponseStatus(code = HttpStatus.OK)
	@PostMapping("/login")
	
	public TokenDto login(@RequestBody LoginDto login) {
		return usuarioService.login(login);

	}

	@PostMapping("/cadastrar")
	@ResponseStatus(code = HttpStatus.CREATED)
	public Usuario setUsuario(@RequestBody Usuario usuario) {
		return usuarioService.setUsuario(usuario);
	}
}
