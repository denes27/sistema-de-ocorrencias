package br.com.itau.sistemaocorrencias.services;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.when;

import java.util.Optional;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;

import br.com.itau.sistemaocorrencias.dtos.LoginDto;
import br.com.itau.sistemaocorrencias.dtos.TokenDto;
import br.com.itau.sistemaocorrencias.enums.Autoridade;
import br.com.itau.sistemaocorrencias.models.Usuario;
import br.com.itau.sistemaocorrencias.repositories.UsuarioRepository;
import br.com.itau.sistemaocorrencias.security.JwtTokenProvider;

@RunWith(SpringRunner.class)
@ContextConfiguration(classes = UsuarioService.class)
public class UsuarioServiceTest {
	
	@Autowired
	private UsuarioService usuarioService;
	
	@MockBean
	private UsuarioRepository usuarioRepository;
	private Usuario usuario;
	
	@MockBean
	private BCryptPasswordEncoder bCrypt;
	
	@Before
	public void preparar() {
		usuario = new Usuario();
		usuario.setAutoridade(Autoridade.USUARIO);
		usuario.setContato("teste@teste.com");
		usuario.setCpf("12345678910");
		usuario.setId(1);
		usuario.setNome("testevaldo dos testes");
		usuario.setSenha("teste123");
	}
	
	@Test
	public void deveRealizarLogin() throws Exception {
		// Given
		LoginDto login = new LoginDto();
		login.setCpf(usuario.getCpf());
		login.setSenha(usuario.getSenha());

		// When
		Mockito.when(usuarioRepository.findByCpf(Mockito.anyString())).thenReturn(Optional.of(usuario));
		Mockito.when(bCrypt.matches(Mockito.anyString(), Mockito.anyString())).thenReturn(true);
		

		TokenDto tokenDto =  usuarioService.login(login);
		String token = tokenDto.getToken();
		
		JwtTokenProvider jwtTokenProvider = new JwtTokenProvider();
		
		// Expected
		assertEquals(tokenDto.getUsuario(), usuario.getCpf());
		assertTrue(jwtTokenProvider.validarToken(token));
		assertEquals(jwtTokenProvider.lerUserToken(token), usuario.getCpf());
		assertEquals(jwtTokenProvider.lerAutoridadeToken(token), usuario.getAutoridade().name());
		
	}

	@Test
        public void deveCriarUmUsuario() {
	    when(usuarioRepository.save(usuario)).thenReturn(usuario);
	  	    
	    Usuario usuarioCriado = usuarioService.setUsuario(usuario);
	    
	    assertEquals(usuario.getNome(), usuarioCriado.getNome());
	    assertEquals(usuario.getId(), usuarioCriado.getId());
	 }
}
