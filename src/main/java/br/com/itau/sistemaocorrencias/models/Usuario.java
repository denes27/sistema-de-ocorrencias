package br.com.itau.sistemaocorrencias.models;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.br.CPF;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonProperty.Access;

import br.com.itau.sistemaocorrencias.enums.Autoridade;

@Entity
public class Usuario {
	
	@Id
	@GeneratedValue (strategy = GenerationType.AUTO)
	private int id;
	
	@Column(name = "Nome")
	@NotBlank(message = "Nome de usuário não inserido!")
	private String nome;
	
	@Column(name = "Contato")
	private String contato;

	@Column(name = "Senha")
	@NotBlank(message = "Senha do usuário não inserida!")
	@JsonProperty (access = Access.WRITE_ONLY)
	private String senha;

	@NotNull
	private Autoridade autoridade;

	@CPF(message = "O CPF inserido é inválido!")
	@Column(name = "Cpf")	
	@NotBlank
	private String cpf;
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public String getContato() {
		return contato;
	}
	public void setContato(String contato) {
		this.contato = contato;
	}
	public String getSenha() {
		return senha;
	}
	public void setSenha(String senha) {
		this.senha = senha;
	}
	public String getCpf() {
		return cpf;
	}
	public void setCpf(String cpf) {
		this.cpf = cpf;
	}
	public Autoridade getAutoridade() {
		return autoridade;
	}
	public void setAutoridade(Autoridade autoridade) {
		this.autoridade = autoridade;
	}
}
