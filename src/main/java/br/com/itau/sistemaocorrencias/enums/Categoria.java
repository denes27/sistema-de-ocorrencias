package br.com.itau.sistemaocorrencias.enums;

public enum Categoria {
	DESASTRE,
	INFRAESTRUTURA,
	CRIME,
	PERTUBACAO_DA_PAZ,
	OUTROS;
	
	public String toString() {
		return this.name().substring(0,1) + this.name().substring(1).toLowerCase();
	}
}
