package br.com.itau.sistemaocorrencias.repositories;

import java.util.Optional;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import br.com.itau.sistemaocorrencias.models.Usuario;

@Repository
public interface UsuarioRepository extends CrudRepository<Usuario, String>{
	public Optional<Usuario> findByCpf(String cpf);
}
