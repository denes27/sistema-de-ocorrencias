package br.com.itau.sistemaocorrencias.security;

import java.util.Date;

import br.com.itau.sistemaocorrencias.enums.Autoridade;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jws;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;

public class JwtTokenProvider {
	private final String chaveSecreta = "aVerySecretKey";
	private final int duracaoEmMilisegundos = 10000000;

	public String criarToken(String userId, Autoridade autoridade) {
		Claims claims = Jwts.claims();

		claims.put("userId", userId);
		claims.put("autoridade", autoridade.name());

		Date agora = new Date();
		Date expiracao = new Date(agora.getTime() + duracaoEmMilisegundos);

		return Jwts.builder().setClaims(claims).setExpiration(expiracao)
				.signWith(SignatureAlgorithm.HS256, chaveSecreta).compact();
	}

	public boolean validarToken(String token) {
		try {
			Jwts.parser().setSigningKey(chaveSecreta).parseClaimsJws(token);

			return true;
		} catch (Exception e) {
			return false;
		}
	}

	public String lerUserToken(String token) {
		Jws<Claims> jwsClaims = Jwts.parser()
				.setSigningKey(chaveSecreta)
				.parseClaimsJws(token);

		return jwsClaims.getBody().get("userId", String.class);

	}

	public String lerAutoridadeToken(String token) {
		Jws<Claims> jwsClaims = Jwts.parser()
				.setSigningKey(chaveSecreta)
				.parseClaimsJws(token);

		return jwsClaims.getBody().get("autoridade", String.class);

	}
}
